#include <bits/stdc++.h>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> g(n);
    for (int i = 0; i < n; ++i) {
        cin >> g[i];
    }
    int q;
    cin >> q;
    vector<int> v(n + 1, 0);
    for (int i = 0; i < q; ++i) {
        int a, b;
        cin >> a >> b;
        v.assign(n + 1, 0);
        for (int j = a; j < b; ++j) {
            v[min(g[j], n)] = 1;
        }
        for (int j = 0;; ++j) {
            if (v[j] == 0) {
                cout << j << "\n";
                break;
            }
        }
    }
}