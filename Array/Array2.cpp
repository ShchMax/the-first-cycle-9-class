#include <bits/stdc++.h>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> g(n);
    for (int i = 0; i < n; ++i) {
        cin >> g[i];
    }
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        int a;
        cin >> a;
        if (a == 1) {
            for (auto p : g) {
                cout << p << " ";
            }
            cout << "\n";
        } else {
            cin >> a;
            g.erase(g.begin() + a);
        }
    }
}
