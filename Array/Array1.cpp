#include <bits/stdc++.h>

using namespace std;

int main() {
    int q;
    cin >> q;
    vector<int> g;
    for (int i = 0; i < q; ++i) {
        int a;
        cin >> a;
        if (a == 1) {
            for (auto p : g) {
                cout << p << " ";
            }
            cout << "\n";
        } else {
            int b;
            cin >> a >> b;
            g.insert(g.begin() + a, b);
        }
    }
}
