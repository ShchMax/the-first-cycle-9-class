#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, a, b;
    cin >> n;
    vector<int> g(n);
    for (int i = 0; i < n; ++i) {
        cin >> g[i];
    }
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        cin >> a;
        if (a == 1) {
            cin >> a >> b;
            int ans = -1e9;
            for (int j = a; j < b; ++j) {
                ans = max(g[j], ans);
            }
            cout << ans << "\n";
        } else {
            cin >> a >> b;
            g[a] = b;
        }
    }
}