#include <bits/stdc++.h>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> g(n);
    for (int i = 0; i < n; ++i) {
        cin >> g[i];
    }
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        int a;
        cin >> a;
        auto p = find(g.begin(), g.end(), a);
        if (p == g.end()) {
            cout << "No\n";
        } else {
            cout << ((*p == a) ? ("Yes") : ("No")) << "\n";
        }
    }
}