#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, a, b;
    cin >> n;
    vector<int> g(n + 1, 0);
    for (int i = 0; i < n; ++i) {
        cin >> a;
        g[i + 1] = g[i] + a;
    }
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        cin >> a >> b;
        cout << g[b] - g[a] << "\n";
    }
}