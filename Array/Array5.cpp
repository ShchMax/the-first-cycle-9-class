#include <bits/stdc++.h>

using namespace std;

int main() {
    vector<int> v;
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        int a, b;
        cin >> a >> b;
        if (a == 1) {
            v.push_back(b);
        } else if (a == 2) {
            v.erase(find(v.begin(), v.end(), b));
        } else {
            cout << ((find(v.begin(), v.end(), b) != v.end()) ? ("Yes") : ("No")) << "\n";
        }
    }
}