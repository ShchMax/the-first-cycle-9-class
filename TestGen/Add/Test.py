"""
Главное не забыть теорему 3 сигм!
"""
from random import *

class Test():
    def get_gauss(self, m, d, n):
        return gauss(m, d) % n
    def rand(self, n):
        return randint(0, n)
    def get_a(self, n, a):
        if (a == 0):
            return self.get_gauss(0, n // 6, n)
        elif (a == 1):
            return self.get_gauss(n // 2, n // 6, n)
        else:
            return self.get_gauss(n, n // 6, n)
    def __init__(self, *a):
        q = int(a[3])
        f = open(a[1], "w")
        print(q, file=f)
        b = self.rand(3)
        n = 0
        for i in range(q):
            if (self.rand(1)):
                print(1, file=f)
            else:
                if (n == 0):
                    print(2, 0, int(self.rand(int(a[2]))), file=f)
                    n += 1
                    continue;
                print(2, int(self.get_a(n, b)), int(self.rand(int(a[2]))), file=f)
                n += 1
        f.close()