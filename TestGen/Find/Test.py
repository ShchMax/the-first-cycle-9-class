"""
Главное не забыть теорему 3 сигм!
"""
from random import *

class Test():
    def get_gauss(self, m, d, n):
        return gauss(m, d) % n
    def rand(self, n):
        return randint(0, n)
    def get_a(self, n, a):
        if (a == 0):
            return self.get_gauss(0, n // 6, n)
        elif (a == 1):
            return self.get_gauss(n // 2, n // 6, n)
        else:
            return self.get_gauss(n, n // 6, n)
    def __init__(self, *a):
        n = self.rand(int(a[0]) - 1) + 1
        q = int(a[3])
        f = open(a[1], "w")
        print(n, file=f)
        print(*[self.rand(int(a[2])) for i in range(n)], file=f)
        print(q, file=f)
        b = self.rand(3)
        for i in range(q):
            print(1, int(self.get_a(n, b)), file=f)
        f.close()