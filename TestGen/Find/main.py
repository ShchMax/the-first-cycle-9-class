from Test import *

for i in range(20): # 0 - 20
    Test(10, "Tests/" + str(i), 10, 10)
for i in range(20): # 20 - 40
    Test(100, "Tests/" + str(i + 20), 100, 100)
for i in range(20): # 40 - 60
    Test(500, "Tests/" + str(i + 40), 500, 500)
for i in range(20): # 60 - 80
    Test(1000, "Tests/" + str(i + 60), 1000, 1000)
for i in range(20): # 80 - 100
    Test(5000, "Tests/" + str(i + 80), 5000, 5000)
for i in range(20): # 100 - 120
    Test(10000, "Tests/" + str(i + 100), 10000, 10000)
for i in range(20): # 120 - 140
    Test(20000, "Tests/" + str(i + 120), 20000, 20000)
for i in range(20): # 140 - 160
    Test(30000, "Tests/" + str(i + 140), 30000, 30000)
for i in range(20): # 160 - 180
    Test(100000, "Tests/" + str(i + 160), 100000, 100000)
for i in range(1, 10):
    for j in range(20):
        Test(100 * i, "Tests/" + str(j + 160 + 20 * i), 100 * i, 100 * i)
for i in range(1, 10):
    for j in range(20):
        Test(10000 * i, "Tests/" + str(j + 340 + 20 * i), 10000 * i, 10000 * i)