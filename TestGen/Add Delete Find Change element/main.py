from Test import *

for i in range(20):
    Test(100, "Tests/" + str(i), 100, 100)
    print((i + 0), "%", sep="")
for i in range(20):
    Test(1000, "Tests/" + str(i + 20), 1000, 1000)
    print((i + 20), "%", sep="")
for i in range(20):
    Test(10000, "Tests/" + str(i + 40), 10000, 10000)
    print((i + 40), "%", sep="")
for i in range(20):
    Test(20000, "Tests/" + str(i + 60), 20000, 20000)
    print((i + 60), "%", sep="")
for i in range(20):
    Test(30000, "Tests/" + str(i + 80), 30000, 30000)
    print((i + 80), "%", sep="")
for i in range(1, 10):
    for j in range(20):
        Test(100 * i, "Tests/" + str(j + 160 + 20 * i), 100 * i, 100 * i)
for i in range(1, 10):
    for j in range(20):
        Test(10000 * i, "Tests/" + str(j + 340 + 20 * i), 10000 * i, 10000 * i)