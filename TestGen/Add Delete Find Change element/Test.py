"""
Главное не забыть теорему 3 сигм!
"""
from random import *

class Test():
    def get_gauss(self, m, d, n):
        return gauss(m, d) % n
    def rand(self, n):
        return randint(0, n)
    def get_a(self, n, a):
        if (a == 0):
            return self.get_gauss(0, n // 6, n)
        elif (a == 1):
            return self.get_gauss(n // 2, n // 6, n)
        else:
            return self.get_gauss(n, n // 6, n)
    def __init__(self, *a):
        q = int(a[3])
        f = open(a[1], "w")
        print(q, file=f)
        b = self.rand(3)
        s = set()
        for i in range(q):
            if (not self.rand(2)):
                if (len(s) == 0 or self.rand(2)):
                    c = self.rand(int(a[2]))
                    print(1, c, file=f)
                    s.add(c)
                    continue
                c = sample(s, 1)[0]
                print(2, c, file=f)
                s.remove(c)
            elif (self.rand(1)):
                c = self.rand(int(a[2]))
                print(1, c, file=f)
                s.add(c)
            else:
                print(3, self.rand(int(a[2])), file=f)
        f.close()