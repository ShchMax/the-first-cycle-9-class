from os import system as bash
from time import time

f = open("Set3.txt", "w")
for i in range(179 + 1):
    print(i / 180 * 100, "%", sep="")
    a = time()
    bash("../Set/Set3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")