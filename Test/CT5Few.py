from os import system as bash
from time import time

f = open("CT5Few.txt", "w")
for i in range(179 + 1, 360):
    print(i / 180 * 100 - 100, "%", sep="")
    a = time()
    bash("../CT/CT5 < ../TestGen/Add\ Delete\ Find\ Change\ element/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")