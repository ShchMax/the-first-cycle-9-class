from os import system as bash
from time import time

f = open("Set5.txt", "w")
for i in range(100):
    print(i, "%", sep="")
    a = time()
    bash("../Set/Set5 < ../TestGen/Add\ Delete\ Find\ Change\ element/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")