from os import system as bash
from time import time

f = open("FT3.txt", "w")
for i in range(179 + 1):
    print(i * 100 / 180, "%", sep="")
    a = time()
    bash("../FT/FT3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")