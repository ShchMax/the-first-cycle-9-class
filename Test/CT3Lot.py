from os import system as bash
from time import time

f = open("CT3Lot.txt", "w")
for i in range(360, 540):
    print(i / 180 * 100 - 200, "%", sep="")
    a = time()
    bash("../CT/CT3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")