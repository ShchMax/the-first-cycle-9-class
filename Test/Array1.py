from os import system as bash
from time import time

f = open("Array1.txt", "w")
for i in range(100):
    print(i, "%", sep="")
    a = time()
    bash("../Array/Array1 < ../TestGen/Add/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")