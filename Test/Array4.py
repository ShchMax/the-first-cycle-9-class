from os import system as bash
from time import time

f = open("Array4.txt", "w")
for i in range(100):
    print(i, "%", sep="")
    a = time()
    bash("../Array/Array4 < ../TestGen/Change/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")