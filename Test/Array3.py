from os import system as bash
from time import time

f = open("Array3.txt", "w")
for i in range(20):
    print(i * 5 / 7, "%", sep="")
    a = time()
    bash("../Array/Array3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
for i in range(20, 40):
    print(i * 5 / 7, "%", sep="")
    a = time()
    bash("../Array/Array3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
for i in range(60, 80):
    print((i - 20) * 5 / 7, "%", sep="")
    a = time()
    bash("../Array/Array3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
for i in range(100, 120):
    print((i - 40) * 5 / 7, "%", sep="")
    a = time()
    bash("../Array/Array3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
for i in range(120, 140):
    print((i - 40) * 5 / 7, "%", sep="")
    a = time()
    bash("../Array/Array3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
for i in range(140, 160):
    print((i - 40) * 5 / 7, "%", sep="")
    a = time()
    bash("../Array/Array3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
for i in range(160, 179 + 1):
    print((i - 40) * 5 / 7, "%", sep="")
    a = time()
    bash("../Array/Array3 < ../TestGen/Find/Tests/" + str(i) + " > /dev/null")
    print((time() - a) * 1000, file=f)
print("Done!")