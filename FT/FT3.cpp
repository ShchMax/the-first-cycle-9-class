#include <bits/stdc++.h>

using namespace std;

class FT {
public:
    FT* left;
    FT* right;
    int val;
    FT(int val)
    : left(nullptr)
    , right(nullptr)
    , val(val)
    {}
    void add(int val) {
        if (this->val > val) {
            if (this->left == nullptr) {
                this->left = new FT(val);
            } else {
                this->left->add(val);
            }
        } else if (this->val < val) {
            if (this->right == nullptr) {
                this->right = new FT(val);
            } else {
                this->right->add(val);
            }
        }
    }
    FT* remove(int val) {
        if (this->val == val) {
            if (this->left == nullptr) {
                if (this->right == nullptr) {
                    return nullptr;
                } else {
                    return this->right;
                }
            } else {
                FT* tmp = this->left;
                while (tmp->left != nullptr) {
                    tmp = tmp->left;
                }
                tmp->left = this->right;
                return this->left;
            }
        } else if (this->val < val) {
            this->right = this->right->remove(val);
        } else {
            this->left = this->left->remove(val);
        }
        return this;
    }
};

bool find(FT* a, int val) {
    if (!a) {
        return false;
    }
    if (a->val == val) {
        return true;
    } else if (a->val < val) {
        return find(a->right, val);
    } else {
        return find(a->left, val);
    }
}

int main() {
    int n;
    cin >> n;
    int a;
    cin >> a;
    FT* root = new FT(a);
    for (int i = 1; i < n; ++i) {
        cin >> a;
        root->add(a);
    }
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        cin >> a;
        if (find(root, a)) {
            cout << "Yes\n";
        } else {
            cout << "No\n";
        }
    }
}