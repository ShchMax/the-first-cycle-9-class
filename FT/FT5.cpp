#include <bits/stdc++.h>

using namespace std;

class FT {
public:
    FT* left;
    FT* right;
    int val;
    FT(int val)
    : left(nullptr)
    , right(nullptr)
    , val(val)
    {}
    void add(int val) {
        if (this->val > val) {
            if (this->left == nullptr) {
                this->left = new FT(val);
            } else {
                this->left->add(val);
            }
        } else if (this->val < val) {
            if (this->right == nullptr) {
                this->right = new FT(val);
            } else {
                this->right->add(val);
            }
        }
    }
};

FT* remove(FT* t, int val) {
    if (t == nullptr) {
        return nullptr;
    }
    if (t->val == val) {
        if (t->left == nullptr) {
            if (t->right == nullptr) {
                return nullptr;
            } else {
                return t->right;
            }
        } else {
            FT* tmp = t->left;
            while (tmp->left != nullptr) {
                tmp = tmp->left;
            }
            tmp->left = t->right;
            return t->left;
        }
    } else if (t->val < val) {
        t->right = remove(t->right, val);
    } else {
        t->left = remove(t->left, val);
    }
    return t;
}

bool find(FT* a, int val) {
    if (!a) {
        return false;
    }
    if (a->val == val) {
        return true;
    } else if (a->val < val) {
        return find(a->right, val);
    } else {
        return find(a->left, val);
    }
}

int main() {
    FT* root = new FT(-1000000000);
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        int a, b;
        cin >> a >> b;
        if (a == 1) {
            root->add(b);
        } else if (a == 2) {
            root = remove(root, b);
        } else {
            if (find(root, b)) {
                cout << "Yes\n";
            } else {
                cout << "No\n";
            }
        }
    }
}