#include <bits/stdc++.h>
#define int long long

using namespace std;

class treap {
public:
    int x, y;
    treap *l, *r;
    treap() {}
    treap(int x)
    : x(x)
    , y(rand())
    , l(0)
    , r(0)
    {}
    treap(int x, int y)
    : x(x)
    , y(y)
    , l(0)
    , r(0)
    {}
};

using beer = treap*; // ПИрамида дереВО

int ansn;
int ansp;

pair<beer, beer> split(beer a, int x) {
    if (!a) {
        return {nullptr, nullptr};
    }
    if (a->x > x) {
        auto p = split(a->l, x);
        a->l = p.second;
        return {p.first, a};
    } else {
        auto p = split(a->r, x);
        a->r = p.first;
        return {a, p.second};
    }
}

beer merge(beer a, beer b) {
    if (!a || !b) {
        return a ? a : b;
    }
    if (a->y < b->y) {
        a->r = merge(a->r, b);
        return a;
    } else {
        b->l = merge(a, b->l);
        return b;
    }
}

beer insert(beer a, int x, int y = rand()) {
    if (!a) {
        return new treap(x, y);
    }
    if (y < a->y) {
        auto p = split(a, x);
        beer v = new treap(x, y);
        v->l = p.first;
        v->r = p.second;
        return v;
    } if (x < a->x) {
        a->l = insert(a->l, x, y);
    } else {
        a->r = insert(a->r, x, y);
    }
    return a;
}

beer erase(beer a, int x) {
    if (!a) {
        return a;
    }
    if (a->x == x) {
        return merge(a->l, a->r);
    } else if (a->x < x) {
        a->r = erase(a->r, x);
        return a;
    } else {
        a->l = erase(a->l, x);
        return a;
    }
}

bool exist(beer a, int x) {
    if (!a) {
        return false;
    } else if (a->x == x) {
        return true;
    }
    if (a->x > x) {
        return exist(a->l, x);
    } else {
        return exist(a->r, x);
    }
}

signed main() {
    beer root = nullptr;
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        int a, b;
        cin >> a >> b;
        if (a == 1) {
            root = insert(root, b);
        } else if (a == 2) {
            root = erase(root, b);
        } else {
            if (exist(root, b)) {
                cout << "Yes\n";
            } else {
                cout << "No\n";
            }
        }
    }
}