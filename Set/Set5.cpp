#include <bits/stdc++.h>

using namespace std;

int main() {
    set<int> v;
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        int a, b;
        cin >> a >> b;
        if (a == 1) {
            v.insert(b);
        } else if (a == 2) {
            v.erase(b);
        } else {
            auto p = v.lower_bound(b);
            if (p != v.end() && *p == b) {
                cout << "Yes\n";
            } else {
                cout << "No\n";
            }
        }
    }
}