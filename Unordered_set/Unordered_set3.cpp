#include <bits/stdc++.h>

using namespace std;

int main() {
    int n;
    cin >> n;
    int a;
    unordered_set<int> s;
    for (int i = 0; i < n; ++i) {
        cin >> a;
        s.insert(a);
    }
    int q;
    cin >> q;
    for (int i = 0; i < q; ++i) {
        cin >> a;
        cout << ((s.find(a) != s.end()) ? ("Yes") : ("No")) << "\n";
    }
}