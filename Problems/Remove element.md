# Задача удаления элемента

## Постановка задачи

Требуется написать программу умеющую удалять элементы в последовательности.
В целях проверки корректности программы требуется выводить последовательность.

## Входные данные

В первой строке вводится длина последовательности.
Во второй строке вводится последовательность.
В третьей строке вводится количество запросов.
Возможные варианты ввода в последующих строках:

    1) 1 - необходимо напечатать всю последовательность.
    2) 2 a - нужно удалить a + 1 элемент.

## Выходные данные

На каждый запрос типа 1) нужно в отдельной строке выводить всю получившуюся последовательность в отдельной строке.